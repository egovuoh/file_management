describe("User Tests", function () {

    it('NS.User.checkToken :: For forgot password', function (done) {
        this.timeout(10000);
        var filter = {
           // studentId: '15MCMC01'
           userId:'15MMPP01'
        }
        var token = 'dssdgd@gmail.com';
        var userId ='15MMPP01';
        
        NS.User.checkToken(token).then(function (res) {
            if (res instanceof Object) {
                done();
            } else {
                done('Error');
            }
        }, function (err) {
            if (err === '') {
                done('Server down.');
            } else {
                done(err);
            }
        });
    });

     it('NS.User.getStudentSupervisorMapping :: Fetches the relation mapping between students and supervisors', function (done) {
        this.timeout(10000);
        var filter = {
           // studentId: '15MCMC01'
           userId:'15MMPP01'
        }
        var select = {student: true, employee: true};
        var join = {student: true, employee: true};
        var sort ={
               id:1
        }
        NS.User.getStudentSupervisorMapping(filter,select ,join ,sort).then(function (res) {
            if (res instanceof Object) {
                done();
            } else {
                done('Error');
            }
        }, function (err) {
            if (err === '') {
                done('Server down.');
            } else {
                done(err);
            }
        });
    });

    it('NS.User.getStudentSupervisorMapping :: Fetches the relation mapping between students and supervisors with false Values', function (done) {
        this.timeout(10000);
        var filter = {
           // studentId: '15MCMC01'
           userId:'15MMPP01'
        }
        var select = {student: false, employee: false};
        var join = {student: false, employee: false};
        var sort ={
               id:1
        }
        NS.User.getStudentSupervisorMapping(filter,select ,join ,sort ).then(function (res) {
            if (res instanceof Object) {
                done();
            } else {
                done('Error');
            }
        }, function (err) {
            if (err === '') {
                done('Server down.');
            } else {
                done(err);
            }
        });
    });

    it('NS.User.getStudentSupervisorMapping :: Fetches the relation mapping between students and supervisors without passing parameters ', function (done) {
        this.timeout(10000);
        var filter = {
           // studentId: '15MCMC01'
           userId:'15MMPP01'
        }
        var select = {student: true, employee: true};
        var join = {student: true, employee: true};
        var sort ={
               id:1
        }
        NS.User.getStudentSupervisorMapping( ).then(function (res) {
            if (res instanceof Object) {
                done();
            } else {
                done('Error');
            }
        }, function (err) {
            if (err === '') {
                done('Server down.');
            } else {
                done(err);
            }
        });
    });
        
        it('NS.User.getStudentSupervisorMapping :: Fetches the relation mapping between students and supervisors with passing one parameter', function (done) {
        this.timeout(10000);
        var filter = {
           // studentId: '15MCMC01'
           userId:'15MMPP01'
        }
        var select = {student: true, employee: true};
        var join = {student: true, employee: true};
        var sort ={
               id:1
        }
        NS.User.getStudentSupervisorMapping(filter ).then(function (res) {
            if (res instanceof Object) {
                done();
            } else {
                done('Error');
            }
        }, function (err) {
            if (err === '') {
                done('Server down.');
            } else {
                done(err);
            }
        });
    });
    it('NS.User.getUserList :: Should get an array of all users', function (done) {
        this.timeout(10000);
        NS.User.getUserList().then(function (res) {
            if (res instanceof Object) {
                done();
            } else {
                done('Response is not an Object.');
            }
        }, function (err) {
            if (err === '') {
                done('Server down.');
            } else {
                done(err);
            }
        });
    });

    it('NS.User.getUserList :: Should get One user', function (done) {
        this.timeout(10000);
        var filter = {
            userId: '15MCMC01'
        }
        NS.User.getUserList(null, filter).then(function (res) {
            if (res instanceof Object && res.data.length === 1) {
                done();
            } else {
                done('Error');
            }
        }, function (err) {
            if (err === '') {
                done('Server down.');
            } else {
                done(err);
            }
        });
    });

    it('NS.User.getUserList :: Should get No things either student or employee', function (done) {
        this.timeout(10000);
        var filter = {
            userId: ''
        }
        NS.User.getUserList(null, filter).then(function (res) {
            if (res instanceof Object && res.data.length ===0 ) {
                done('Error');
            } else {
                done();
            }
        }, function (err) {
            if (err === '') {
                done('Server down.');
            } else {
                done(err);
            }
        });
    });

it('NS.User.getUserList :: Should get an error because of Wrong employee Id', function (done) {
        this.timeout(10000);
        var filter = {
            userId: '00jHsz1220'
        }
        NS.User.getUserList(null, filter).then(function (res) {
            if (res instanceof Object && res.data.length ===1 ) {
                done('Error');
            } else {
                done();
            }
        }, function (err) {
            if (err === '') {
                done('Server down.');
            } else {
                done(err);
            }
        });
    });

    it('NS.User.getCourses :: Student Should have User ID by only passing type & filter', function (done) {
        this.timeout(10000);
        var filter = {
            userId: '15MCMC01'
        }
        var type = "student"
        NS.User.getUserList(type, filter).then(function (res) {
            if (res instanceof Object && res.data.length ===1 ) {
                done();
            } else {
                done('Error');
            }
        }, function (err) {
            if (err === '') {
                done('Server down.');
            } else {
                done(err);
            }
        });
    });
        
        //===================== * get user List (Employee) * ========================//
    it('NS.User.getCourses :: Employee Should have User ID by passing four parameters', function (done) {
        this.timeout(10000);
        var filter = {
            userId: 'ceofficeuser1'
        }
        var select =undefined;
        var join = undefined;
        var type = 'employee';
        NS.User.getUserList(type, filter,select , join).then(function (res) {
            if (res instanceof Object && res.data.length ===1 ) {
                done();
            } else {
                done('Error');
            }
        }, function (err) {
            if (err === '') {
                done('Server down.');
            } else {
                done(err);
            }
        });
    });

     //===================== * get user List (Student) * ========================//
    it('NS.User.getCourses :: Student Should have User ID & passing four parameters', function (done) {
        this.timeout(10000);
        var filter = {
            userId: '15MCMC01'
        }
        var select =undefined;
        var join = undefined;
        var type = 'student';
        NS.User.getUserList(type, filter,select , join).then(function (res) {
            if (res instanceof Object && res.data.length ===1 ) {
                done();
            } else {
                done('Error');
            }
        }, function (err) {
            if (err === '') {
                done('Server down.');
            } else {
                done(err);
            }
        });
    });

     //===================== * get user List (Student) * ========================//
    it('NS.User.getCourses :: Employee Should have User ID & passing four difference parameters Values', function (done) {
        this.timeout(10000);
        var filter = {
            userId: '00FWtg22'            
        }
        var select ='STU_USER';
        var join = 'STU_USER';
        var type = undefined;
        NS.User.getUserList(type, filter,select,join).then(function (res) {
            if (res instanceof Object && res.data.length ===1 ) {
                done();
            } else {
                done('Error');
            }
        }, function (err) {
            if (err === '') {
                done('Server down.');
            } else {
                done(err);
            }
        });
    });

      //===================== * mapStudentSupervisor * ========================//
   /*  it('NS.User.mapStudentSupervisor :: It Should maps students to supervisors by passing all parameters ', function (done) {
        this.timeout(20000);
         var filter = {
             userId: '15MCMC01'
         }
         var type="backlogs";
         var supervisorId="";
         var supervisorType='teacher';
         var studentIdArr=['Hyderabad Telangana'];
         var efftFrom='2017-01-30';
         var efftTo ='2017-06-30' ;
       NS.User.mapStudentSupervisor(supervisorId,supervisorType,studentIdArr,efftFrom,efftTo).then(function (res) {
            if (res instanceof Object) {
                done();
            } else {
                done('Error');
            }
        }, function (err) {
            if (err === '') {
                done('Server down.');
            } else {
                done(err);
            }
        });
    });
    */
     //===================== * get Courses * ========================//
     it('NS.User.getCourses :: User get Course ', function (done) {
        this.timeout(20000);
        var filter = {
            semester:2
        }
       // var type='backlogs';
        var type='improvement';
        var getC = new NS.User();
        getC.id='15MCMC01';
       getC.getCourses(type, filter).then(function (res) {
            // console.log(JSON.stringify(res));
            // done();
            if (res instanceof Object) {
                done();
            } else {
                done('Error');
            }
        }, function (err) {
            if (err === '') {
                done('Server down.');
            } else {
                done(err);
            }
        });
    });

     //===================== * getNotifications * ========================//
     it('NS.User.getNotifications :: User Notifications ', function (done) {
        this.timeout(20000);
        var filter = {
           isDeleted:true,
           isRead:true
        }
        var getNot = new NS.User();
        getNot.id='15MCMC01';
        getNot.getNotifications(filter).then(function (res) {
            if (res instanceof Object) {
                done();
            } else {
                done('Error');
            }
            //  console.log(JSON.stringify(res));
            // done();
        }, function (err) {
            if (err === '') {
                done('Server down.');
            } else {
                done(err);
            }
        });
    });


     //===================== * getResults * ========================//
     it('NS.User.getResults ::It Should get User result ', function (done) {
        this.timeout(20000);
         var getR = new NS.User();
        getR.id='15MCMC01';
        var key='MCA';
       
       getR.getResults(key).then(function (res) {
            if (res instanceof Object ) {
                done();
            } else {
                done('Error');
            }
            // console.log(JSON.stringify(res));
            // done();
        }, function (err) {
            if (err === '') {
                done('Server down.');
            } else {
                done(err);
            }
        });
    });
     //===================== * LogIn Function * ========================//
     it('NS.User.LogIn :: User Log In With right credential ', function (done) {
        this.timeout(20000);
        var logI = new NS.User();
        logI.id='15MCMC01';
        logI.password='abcd123';
       logI.LogIn().then(function (res) {
            if (res instanceof Object ) {
                done();
            } else {
                done('Error');
            }
            // console.log(JSON.stringify(res));
            // done();
        }, function (err) {
            if (err === '') {
                done('Server down.');
            } else {
                done(err);
            }
        });
    });


    //===================== * logOut Function * ========================//
     it('NS.User.LogOut :: User Logout', function (done) {
        this.timeout(20000);
         var logO = new NS.User();
        logO.id='15MCMC01';
       logO.LogOut().then(function (res) {
            if (res instanceof Object ) {
                done();
            } else {
                done('Error');
            }
            // console.log(JSON.stringify(res));
            // done();
        }, function (err) {
            if (err === '') {
                done('Server down.');
            } else {
                done(err);
            }
        });
    });


});
