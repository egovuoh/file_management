var savedResults = {};
describe("Event Tests", function () {
    // --------------------------------------------------------------------------- //
    // ------------------------ NS.Event.getEvent() ------------------------------ //
    describe("NS.Event.getEvent", function () {
        it('NS.Event.getEvent :: Should get All Events', function (done) {
            this.timeout(10000);
            NS.Event.getEvent().then(function (res) {
                if (res instanceof Array) {
                    for (var i = 0; i < res.length; i++) {
                        if (!res[i] instanceof NS.Event) {
                            done('It is not a instance of NS.Event');
                        }
                    }
                    done();
                } else if (res.noEvents) {
                    done();
                } else {
                    done('Error');
                }
            }, function (err) {
                if (err === '') {
                    done('Server down.');
                } else {
                    done(err);
                }
            });
        });

        it('NS.Event.getEvent :: Should get only one Event', function (done) {
            this.timeout(10000);
            var filter = {
                eventId: 45
            }
            NS.Event.getEvent(filter).then(function (res) {
                if (res instanceof Array && res.length === 1) {
                    done();
                } else {
                    done('Error');
                }
            }, function (err) {
                if (err === '') {
                    done('Server down.');
                } else {
                    done(err);
                }
            });
        });

        it('NS.Event.getEvent :: Should fetch events even with wrong type. Default behaviour', function (done) {
            this.timeout(10000);
            var filter = {
                eventId: 45
            }
            NS.Event.getEvent(filter, filter).then(function (res) {
                if (res instanceof Array) {
                    for (var i = 0; i < res.length; i++) {
                        if (!res[i] instanceof NS.Event) {
                            done('It is not a instance of NS.Event');
                        }
                    }
                    done();
                } else if (res.noEvents) {
                    done();
                } else {
                    done('Error');
                }
            }, function (err) {
                if (err === '') {
                    done('Server down.');
                } else {
                    done(err);
                }
            });
        });

        it('NS.Event.getEvent :: Should fetch events with computed occurrences for calendar.', function (done) {
            this.timeout(10000);
            var type = 'withOccurrences';
            var filter = {
                eventStart: "2017-05-01 00:00:00",
                eventEnd: "2017-05-20 00:00:00"
            }
            NS.Event.getEvent(filter, type).then(function (res) {
                if (res instanceof Array) {
                    for (var i = 0; i < res.length; i++) {
                        if (!res[i] instanceof NS.Event) {
                            done('It is not a instance of NS.Event');
                        }
                    }
                    done();
                } else if (res.noEvents) {
                    done();
                } else {
                    done('Error');
                }
            }, function (err) {
                if (err === '') {
                    done('Server down.');
                } else {
                    done(err);
                }
            });
        });

        it('NS.Event.getEvent :: Should get an Error because of passing invalid date formats', function (done) {
            this.timeout(10000);
            var type = 'withOccurrences';
            var filter = {
                eventStart: "2017-05-01 ",
                eventEnd: "2017-05-20 "
            }
            NS.Event.getEvent(filter, type).then(function (res) {
                done('It should not have passed');
            }, function (err) {
                if (err === '') {
                    done('Server down.');
                } else if (err.error === 'INVALID_DATE') {
                    done();
                } else {
                    done(err);
                }
            });
        });

        it('NS.Event.getEvent :: Should get an Error because of empty Date', function (done) {
            this.timeout(10000);
            var type = 'withOccurrences';
            var filter = {
                eventStart: " ",
                eventEnd: " "
            }
            NS.Event.getEvent(filter, type).then(function (res) {
                if (res instanceof Array) {
                    done('Error');
                } else {
                    done();
                }
            }, function (err) {
                if (err === '') {
                    done('Server down.');
                } else if (err.error === 'INVALID_DATE') {
                    done();
                } else {
                    done(err);
                }
            });
        });

        it('NS.Event.getEvent :: Should get an Error because missing parameters', function (done) {
            this.timeout(20000);
            var type = 'withOccurrences';
            var filter = {
                eventEnd: "2017-05-20"
            }
            NS.Event.getEvent(filter, type).then(function (res) {
                done('Should not have passed');
            }, function (err) {
                if (err === '') {
                    done('Server down.');
                } else if (err.error === 'MISSING_PARAMS') {
                    done();
                } else {
                    done(err);
                }
            });
        });


        it('NS.Event.getEvent :: Should get an empty array for unknown event id or when No event was found', function (done) {
            this.timeout(20000);
            var filter = {
                eventId: 1
            }
            NS.Event.getEvent(filter).then(function (res) {
                if (res instanceof Array && res.length === 0) {
                    done();
                } else {
                    done('Response is not array or length > 0');
                }
            }, function (err) {
                if (err === '') {
                    done('Server down.');
                } else {
                    done(err);
                }
            });
        });

        it('NS.Event.getEvent :: Should get an empty array when no event defined for given time-range', function (done) {
            this.timeout(20000);
            var filter = {
                eventStart: '2030-01-01 00:00:00',
                eventEnd: '2030-12-01 00:00:00'
            }
            var type = 'withOccurrences';
            NS.Event.getEvent(filter, type).then(function (res) {
                if (res instanceof Array && res.length === 0) {
                    done();
                } else {
                    done('Response is not array or length > 0');
                }
            }, function (err) {
                if (err === '') {
                    done('Server down.');
                } else {
                    done(err);
                }
            });
        });

    });
    // --------------------------------------------------------------------------- //
    // --------------------------- getSubscribers() ------------------------------ //
    describe("NS.Event.prototype.getSubscribers", function () {
        it('Should get all subscribers for event 45', function (done) {
            this.timeout(20000);
            var filter = {
                eventId: 45
            }
            NS.Event.getEvent(filter).then(function (res) {
                //res.is of type NS.Event
                if (res instanceof Array && res.length === 1 && res[0] instanceof NS.Event) {
                    //proceed to fetch subscribers
                    var event = res[0];
                    event.getSubscribers().then(function (res2) {
                        if (res2 instanceof Object) {
                            done();
                        }
                    }, function (err2) {
                        done(err2);
                    });
                } else {
                    done('Not of type NS.Event');
                }
            }, function (err) {
                if (err === '') {
                    done('Server down.');
                } else {
                    done(err);
                }
            });
        });

        it('Should get all subscribers for event 45 with users resolved', function (done) {
            this.timeout(20000);
            var filter = {
                eventId: 45
            }
            NS.Event.getEvent(filter).then(function (res) {
                //res.is of type NS.Event
                if (res instanceof Array && res.length === 1 && res[0] instanceof NS.Event) {
                    //proceed to fetch subscribers
                     var event = res[0];
                    event.getSubscribers(undefined, true).then(function (res2) {
                        if (res2.data instanceof Array) {
                            done();
                        } else {
                            done('Expected Array, found ' + (typeof res2.data));
                        }
                    }, function (err2) {
                        done(err2);
                    });
                } else {
                    done('Not of type NS.Event');
                }
            }, function (err) {
                if (err === '') {
                    done('Server down.');
                } else {
                    done(err);
                }
            });
        });

        it('Should get all subscribers for event 45 with resolveUsers as false', function (done) {
            this.timeout(20000);
            var filter = {
                eventId: 45
            }
            NS.Event.getEvent(filter).then(function (res) {
                //res.is of type NS.Event
                if (res instanceof Array && res.length === 1 && res[0] instanceof NS.Event) {
                    //proceed to fetch subscribers
                    var event = res[0];
                    event.getSubscribers(undefined, false).then(function (res2) {
                        if (res2.data instanceof Object) {
                            done();
                        } else {
                            done('Expected Object, found ' + (typeof res2.data));
                        }
                    }, function (err2) {
                        done(err2);
                    });
                } else {
                    done('Not of type NS.Event');
                }
            }, function (err) {
                if (err === '') {
                    done('Server down.');
                } else {
                    done(err);
                }
            });
        });

        //     it('Should giv a message no subscribers found for id 47', function (done) {
        //         this.timeout(20000);
        //         var filter = {
        //             eventId: 45    //47
        //         }
        //         NS.Event.getEvent(filter).then(function (res) {
        //             //res.is of type NS.Event
        //             console.log(JSON.stringify(res));
        //             if (res instanceof Array && res.length >= 0 && res[0] instanceof NS.Event) {
        //                 //proceed to fetch subscribers
        //                 var event = res[0];
        //                 event.getSubscribers().then(function (res2) {
        //                     if (res2.message === 'No Subscribers found.') {
        //                         done('Error');
        //                     }
        //                 }, function (err2) {
        //                     done();
        //                 });
        //             } else {
        //                 done('Not Found or Not of type NS.Event');
        //             }
        //         }, function (err) {
        //             if (err === '') {
        //                 done('Server down.');
        //             } else {
        //                 done();
        //             }
        //         });
        //     });


        //     it('Should get subscribers of type user for event 45', function (done) {
        //         this.timeout(20000);
        //         var filter = {
        //             eventId: 45
        //         }
        //         NS.Event.getEvent(filter).then(function (res) {
        //             //res.is of type NS.Event
        //             if (res instanceof Array && res.length >= 0 && res[0] instanceof NS.Event) {
        //                 //proceed to fetch subscribers
        //                 var filter2 = {
        //                     type: 'user'
        //                 };
        //                 var event = res[0];
        //                 event.getSubscribers(filter2).then(function (res2) {
        //                     //check
        //                 }, function (err2) {
        //                     done(err2);
        //                 });
        //             } else {
        //                 done('Not of type NS.Event');
        //             }getSub
        //         }, function (err) {
        //             if (err === '') {
        //                 done('Server down.');
        //             } else {
        //                 done();
        //             }
        //         });
        //     });

        it('Should get all subscribers for event 45 with and length les than Zero', function (done) {
            this.timeout(20000);
            var filter = {
                eventId: 45
            }
            NS.Event.getEvent(filter).then(function (res) {
                //res.is of type NS.Event
                if (res instanceof Array && res.length >= 0 && res[0] instanceof NS.Event) {
                    //proceed to fetch subscribers
                    var event = res[0];
                    event.getSubscribers().then(function (res2) {
                        if (res2.data instanceof Object) {
                            done();
                        } else {
                            done('Expected Object, found ' + (typeof res2.data));
                        }
                    }, function (err2) {
                        done(err2);
                    });
                } else {
                    done('Not of type NS.Event');
                }
            }, function (err) {
                if (err === '') {
                    done('Server down.');
                } else {
                    done(err);
                }
            });
        });

        it('Should get all users subscribers for event 45 with ', function (done) {
            this.timeout(20000);
            var filter = {
                eventId: 45
            }
            NS.Event.getEvent(filter).then(function (res) {
                //res.is of type NS.Event
                if (res instanceof Array && res.length >= 0 && res[0] instanceof NS.Event) {
                    //proceed to fetch subscribers
                    var event = res[0];
                    event.getSubscribers().then(function (res2) {
                        if (res2.data instanceof Object) {
                            done(); 

                            } else {
                                    done('Expected Object, found ' + (typeof res2.data));
                        }
                    }, function (err2) {
                        done(err2);
                    });
                } else {
                    done('Not of type NS.Event');
                }
            }, function (err) {
                if (err === '') {
                    done('Server down.');
                } else {
                    done(err);
                }
            });
        });

        it('Should get all groups subscribers for event 45 with ', function (done) {
            this.timeout(20000);
            var filter = {
                eventId: 45
            }
            NS.Event.getEvent(filter).then(function (res) {
                //res.is of type NS.Event
                if (res instanceof Array && res.length === 1 && res[0] instanceof NS.Event) {
                    //proceed to fetch subscribers
                    var event = res[0];
                    event.getSubscribers().then(function (res2) {
                        if (res2.data instanceof Object) {
                            done();
                        } else {
                            done('Expected Object, found ' + (typeof res2.data));
                        }
                    }, function (err2) {
                        done(err2);
                    });
                } else {
                    done('Not of type NS.Event');
                }
            }, function (err) {
                if (err === '') {
                    done('Server down.');
                } else {
                    done(err);
                }
            });
        });



    });

    // --------------------------------------------------------------------------- //
    // --------------------------- saveEvent() ------------------------------ //
    describe("NS.Event.prototype.saveEvent", function () {
        it('Should save an event', function (done) {
            this.timeout(10000);
            var testEvent = new NS.Event();
            testEvent.eventTitle = 'My Testing Event';
            testEvent.eventStart = new Date('2017-05-30 00:00:00');
            testEvent.eventEnd = new Date('2017-05-31 00:00:00');
            testEvent.eventCreator = '15MCMC01';
            testEvent.saveEvent().then(function (res) {
                savedResults.savedEventId = res.eventId;
                console.log(res.eventId);
                done();
            }, function (err) {
                done(err);
            });
        });
        it('NS.Event.getEvent :: Should get only one Event', function (done) {
            this.timeout(10000);
            console.log('Fetching event ' + savedResults.savedEventId);
            var filter = {
                eventId: savedResults.savedEventId
            }
            NS.Event.getEvent(filter).then(function (res) {
                if (res instanceof Array && res.length === 1) {
                    done();
                } else {
                    done('Error');
                }
            }, function (err) {
                if (err === '') {
                    done('Server down.');
                } else {
                    done(err);
                }
            });
        });

        // it('Should get all subscribers ', function (done) {
        //     this.timeout(20000);
        //     var filter = {
        //         eventId: savedResults.savedEventId
        //     }
        //     NS.Event.getEvent(filter).then(function (res) {
        //         //res.is of type NS.Event
        //         if (res instanceof Array && res.length === 1 && res[0] instanceof NS.Event) {
        //             //proceed to fetch subscribers
        //              var event = res[0];
        //              event.getSubscribers().then(function (res2) {
        //                 if (res2 instanceof Object) {
        //                     don();
        //                 }
        //             }, function (err2) {
        //                 done(err2);
        //             });
        //         } else {
        //             done('Not of type NS.Event');
        //         }
        //     }, function (err) {
        //         if (err === '') {
        //             done('Server down.');
        //         } else {
        //             done(err);
        //         }
        //     });
        // });


        it('Should add subscribers for few users to the event', function (done) {
            this.timeout(20000);
          //  console.log('Fetching event ' + savedResults.savedEventId);
            // var type = 'withOccurrences';
            var filter = {
                eventId: savedResults.savedEventId
            }
             NS.Event.getEvent(filter).then(function (res) {
                //res.is of type NS.Event
                if (res instanceof Array && res.length === 1 && res[0] instanceof  NS.Event) {
                    //proceed to fetch subscribers
                    // var event = res[0];
                    var subscribers = {
                        users: ['15MCMC01', '15MCMC02', '15MCMC03'],
                        groups: [],
                        exUsers: [],
                        exGroups: []
                    }
                      var event = res[0];
                    event.subscribe(subscribers).then(function (res2) {
                        if (res2 instanceof Object) {
                            done();
                        } else {
                            done('Error');
                        }
                    }, function (err) {
                        if (err === '') {
                            done('Server down.');
                        } else {
                            done(err);
                        }
                    });
                } else {
                    done('Error');
                }
            }, function (err) {
                done(err);
            });
        });
    });

});
