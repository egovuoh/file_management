describe("Class Assets", function () {

    it('NS.Asset.get :: It should fetch Asset', function (done) {
        this.timeout(10000);
        var filter = {
           userId:'15MMPP01'
        }       
       NS.Asset.get().then(function (res) {
            if (res instanceof Array) {
                done();
            } else {
                done('Error');
            }
        }, function (err) {
            if (err === '') {
                done('Server down.');
            } else {
                done(err);
            }
        });
    });

});