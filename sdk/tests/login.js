describe("Login Tests",function(done){

    it("Should Login and Return an User object", function(done){
        this.timeout(10000);

        var user = new NS.User();
        user.id = "15MCMC01";
        user.password = "abcd123";
        user.LogIn().then(function(user){
            done();
        },function(err){
            done(err);
        });
    });

    it("Should enter correct credential user Id & Password", function(done){
        this.timeout(10000);

        var user = new NS.User();
        user.id = "25MCMC01";
        user.password = "abcd12333";
        user.LogIn().then(function(user){
            done(err);
        },function(err){
            done();
        });
    });

    it("Should enter correct Password", function(done){
        this.timeout(10000);

        var user = new NS.User();
        user.id = "15MCMC01";
        user.password = "abcd12333";
        user.LogIn().then(function(user){
            done(err);
        },function(err){
            done();
        });
    });

    it("Should enter Password It will not be blank", function(done){
        this.timeout(10000);

        var user = new NS.User();
        user.id = "15MCMC01";
        user.password = "";
        try{
            user.LogIn().then(function(user){
            done(err);
        },function(err){
            done(err);
        },function(err){
            done(err);
        });  
        }catch(e){
            done();
        }
        
        });

    it("Should Enter user ID And Password", function(done){
        this.timeout(10000);

        var user = new NS.User();
        user.id = "";
        user.password = "";
        try{
            user.LogIn().then(function(user){
                done(err);
            },function(err){
                done(err);
            });
        } catch (e){
            done();
        }
    });

    it("Should Enter user ID And forgot his Password", function(done){
        this.timeout(10000);

        var user = new NS.User();
        user.id = "15MCMC01";
        user.password = "";
        try{
            user.LogIn().then(function(user){
                done(err);
            },function(err){
                done(err);
            });
        } catch (e){
            done();
        }
    });

});