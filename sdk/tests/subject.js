describe("Subject Tests", function () {

       // --------------------------------------------------------------------------- //
       // --------------------------- fetch subject() ------------------------------ //

//   it('NS.Subject.fetch :: Should fetch all subject check instance of NS.Subject', function (done) {
//         this.timeout(10000);
//         NS.Subject.fetch().then(function (res) {  
//             if (res instanceof Array && res instanceof NS.Subject) {
//                 done(); 
//             }else {
//             done('Error');
//         }
//         }, function (err) {
//             if (err === '') {
//                 done('Server down.');
//             } else {
//                 done(err);
//             }
//         });
//     });

    it('NS.Subject.fetch ::  Should fetch all subject which is an Array without passing parameters', function (done) {
            this.timeout(10000);
            // id, schoolId, deptId, semester, type
        
            NS.Subject.fetch().then(function (res) {
                if (res instanceof Array) {
                    done();
                } else {
                    done('Error');
                }
            }, function (err) {
                if (err === '') {
                    done('Server down.');
                } else {
                    done(err);
                }
            });
        });
    

    it('NS.Subject.fetch ::  Should fetch specific subject with passing all parameters and which is an array', function (done) {
            this.timeout(10000);
            // id, schoolId, deptId, semester, type
            var id="IH500";
            var schoolId= "sosc123";
            var deptId= "doh123";
            var semester= 2;
            var type = "core";
            NS.Subject.fetch(id,schoolId,deptId,semester,type).then(function (res) {
                if (res instanceof Array) {
                     for(var i=0;i<res.length;i++)
                    {
                        if(res[i].id==="IH500" && res[i].schoolId==="sosc123" && res[i].deptId==="doh123" && res[i].semester===2 && res[i].type==="core")
                        {

                        }
                        else{
                            done("Error dose Not match requirement ");
                        }
                    }
                    done();
                } else {
                    done('Error');
                }
            }, function (err) {
                if (err === '') {
                    done('Server down.');
                } else {
                    done(err);
                }
            });
        });

        it('NS.Subject.fetch ::  Should fetch  subject with passing four parameters and which is an array', function (done) {
            this.timeout(10000);
            // id, schoolId, deptId, semester, type
            var id="IH500";
            var schoolId= "sosc123";
            var deptId= "doh123";
            var semester= 2;
            // var type = "core";
            NS.Subject.fetch(id,schoolId,deptId,semester).then(function (res) {
                if (res instanceof Array) {
                    for(var i=0;i<res.length;i++)
                    {
                        if(res[i].id==="IH500" && res[i].schoolId==="sosc123" && res[i].deptId==="doh123" && res[i].semester===2)
                        {

                        }
                        else{
                            done("Error dose Not match requirement ");
                        }
                    }
                    done();
                } else {
                    done('Error');
                }
            }, function (err) {
                if (err === '') {
                    done('Server down.');
                } else {
                    done(err);
                }
            });
        });

        it('NS.Subject.fetch ::  Should fetch  subject with passing four parameters and which is an array', function (done) {
            this.timeout(10000);
            // id, schoolId, deptId, semester, type
            var schoolId= "sosc123";
            var deptId= "doh123";
            var semester= 2;
            var type = "core";
            NS.Subject.fetch(schoolId,deptId,semester,type).then(function (res) {
                if (res instanceof Array) {
                    for(var i=0;i<res.length;i++)
                    {
                        if(res[i].schoolId==="sosc123" && res[i].deptId==="doh123" && res[i].semester===2 && res[i].type==="core")
                        {

                        }
                        else{
                            done("Error dose Not match requirement ");
                        }
                    }
                    done();
                } else {
                    done('Error');
                }
            }, function (err) {
                if (err === '') {
                    done('Server down.');
                } else {
                    done(err);
                }
            });
        });

        it('NS.Subject.fetch ::  Should fetch  subject with passing four parameters and which is an array', function (done) {
            this.timeout(10000);
            // id, schoolId, deptId, semester, type
            var id="IH500";
            var deptId= "doh123";
            var semester= 2;
            var type = "core";
            NS.Subject.fetch(id,deptId,semester,type).then(function (res) {
                if (res instanceof Array) {
                    for(var i=0;i<res.length;i++)
                    {
                        if(res[i].id==="IH500" &&  res[i].deptId==="doh123" && res[i].semester===2 && res[i].type==="core")
                        {

                        }
                        else{
                            done("Error dose Not match requirement ");
                        }
                    }
                    done();
                } else {
                    done('Error');
                }
            }, function (err) {
                if (err === '') {
                    done('Server down.');
                } else {
                    done(err);
                }
            });
        });

        it('NS.Subject.fetch ::  Should fetch  subject with passing four parameters and which is an array', function (done) {
            this.timeout(10000);
            // id, schoolId, deptId, semester, type
            var id="IH500";
            var schoolId= "sosc123";
            var deptId= "doh123";
            var type = "core";
            NS.Subject.fetch(id,schoolId,deptId,type).then(function (res) {
                if (res instanceof Array) {
                    for(var i=0;i<res.length;i++)
                    {
                        if(res[i].id==="IH500" && res[i].schoolId==="sosc123" && res[i].deptId==="doh123" && res[i].type==="core")
                        {

                        }
                        else{
                            done("Error dose Not match requirement ");
                        }
                    }
                    done();
                } else {
                    done('Error');
                }
            }, function (err) {
                if (err === '') {
                    done('Server down.');
                } else {
                    done(err);
                }
            });
        });

        it('NS.Subject.fetch ::  Should fetch  subject with passing four parameters and which is an array', function (done) {
            this.timeout(10000);
            // id, schoolId, deptId, semester, type
            var id="IH500";
            var schoolId= "sosc123";
            var semester= 2;
            var type = "core";
            NS.Subject.fetch(id,schoolId,semester,type).then(function (res) {
                if (res instanceof Array) {
                    for(var i=0;i<res.length;i++)
                    {
                        if(res[i].id==="IH500" && res[i].schoolId==="sosc123" && res[i].semester===2 && res[i].type==="core")
                        {

                        }
                        else{
                            done("Error dose Not match requirement ");
                        }
                    }
                    done();
                } else {
                    done('Error');
                }
            }, function (err) {
                if (err === '') {
                    done('Server down.');
                } else {
                    done(err);
                }
            });
        });





         it('NS.Subject.fetch ::  Should fetch  subject with passing three parameters schoolId , deptId & semester', function (done) {
            this.timeout(10000);
            // id, schoolId, deptId, semester, type
            var schoolId= "sosc123";
            var deptId= "doh123";
            var semester= 2;
            NS.Subject.fetch(schoolId,deptId,semester).then(function (res) {
                if (res instanceof Array) {
                    for(var i=0;i<res.length;i++)
                    {
                        if(res[i].schoolId==="sosc123" && res[i].deptId==="doh123" && res[i].semester===2)
                        {

                        }
                        else{
                            done("Error dose Not match requirement ");
                        }
                    }
                    done();
                } else {
                    done('Error');
                }
            }, function (err) {
                if (err === '') {
                    done('Server down.');
                } else {
                    done(err);
                }
            });
        });

         it('NS.Subject.fetch ::  Should fetch  subject with passing three parameters schoolId , deptId & type', function (done) {
            this.timeout(10000);
            // id, schoolId, deptId, semester, type
            var schoolId= "sosc123";
            var deptId= "doh123";
            var type = "core";
            NS.Subject.fetch(schoolId,deptId, type).then(function (res) {
                if (res instanceof Array) {
                    for(var i=0;i<res.length;i++)
                    {
                        if(res[i].schoolId==="sosc123" && res[i].deptId==="doh123" && res[i].type==="core")
                        {

                        }
                        else{
                            done("Error dose Not match requirement ");
                        }
                    }
                    done();
                } else {
                    done('Error');
                }
            }, function (err) {
                if (err === '') {
                    done('Server down.');
                } else {
                    done(err);
                }
            });
        });

        it('NS.Subject.fetch ::  Should fetch  subject with passing three parameters id , schoolId  & semester', function (done) {
            this.timeout(10000);
            // id, schoolId, deptId, semester, type
            var id ="IH500";
            var schoolId= "sosc123";
            var semester= 2;
            NS.Subject.fetch(id,schoolId,semester).then(function (res) {
                if (res instanceof Array) {
                    for(var i=0;i<res.length;i++)
                    {
                        if(res[i].id==="IH500" && res[i].schoolId==="sosc123" && res[i].semester===2)
                        {

                        }
                        else{
                            done("Error dose Not match requirement ");
                        }
                    }
                    done();
                } else {
                    done('Error');
                }
            }, function (err) {
                if (err === '') {
                    done('Server down.');
                } else {
                    done(err);
                }
            });
        });

        it('NS.Subject.fetch ::  Should fetch  subject with passing three parameters deptId ,semester  & type ', function (done) {
            this.timeout(10000);
            // id, schoolId, deptId, semester, type
            var deptId= "doh123";
            var semester= 2;
            var type = "core";
            NS.Subject.fetch(deptId,semester,type).then(function (res) {
                if (res instanceof Array) {
                    for(var i=0;i<res.length;i++)
                    {
                        if(res[i].deptId==="doh123" && res[i].semester===2 && res[i].type==="core")
                        {

                        }
                        else{
                            done("Error dose Not match requirement ");
                        }
                    }
                    done();
                } else {
                    done('Error');
                }
            }, function (err) {
                if (err === '') {
                    done('Server down.');
                } else {
                    done(err);
                }
            });
        });

        it('NS.Subject.fetch ::  Should fetch  subject with passing three parameters and which is an array', function (done) {
            this.timeout(10000);
            // id, schoolId, deptId, semester, type
            var id="IH500";
            var schoolId= "sosc123";
            var deptId= "doh123";
            // var semester= 2;
            // var type = "core";
            NS.Subject.fetch(id,schoolId,deptId).then(function (res) {
                if (res instanceof Array) {
                     for(var i=0;i<res.length;i++)
                    {
                        if(res[i].id==="IH500" && res[i].schoolId==="sosc123" && res[i].deptId==="doh123")
                        {

                        }
                        else{
                            done("Error dose Not match requirement ");
                        }
                    }
                    done();
                } else {
                    done('Error');
                }
            }, function (err) {
                if (err === '') {
                    done('Server down.');
                } else {
                    done(err);
                }
            });
        });


            it('NS.Subject.fetch ::  Should fetch  subject with passing two parameters and which is an array', function (done) {
            this.timeout(10000);
            // id, schoolId, deptId, semester, type
            var id="IH500";
            var schoolId= "sosc123";
            NS.Subject.fetch(id,schoolId).then(function (res) {
                if (res instanceof Array) {
                     for(var i=0;i<res.length;i++)
                    {
                        if(res[i].id==="IH500" && res[i].schoolId==="sosc123" )
                        {

                        }
                        else{
                            done("Error dose Not match requirement ");
                        }
                    }
                    done();
                } else {
                    done('Error');
                }
            }, function (err) {
                if (err === '') {
                    done('Server down.');
                } else {
                    done(err);
                }
            });
        });

        

         
         it('NS.Subject.fetch ::  Should fetch  subject with passing two parameters id & deptId', function (done) {
            this.timeout(10000);
            // id, schoolId, deptId, semester, type
            var id="IH500";
            var deptId= "doh123";
            NS.Subject.fetch(id,deptId).then(function (res) {
                if (res instanceof Array) {
                    for(var i=0;i<res.length;i++)
                    {
                        if(res[i].id==="IH500" && res[i].deptId==="doh123")
                        {

                        }
                        else{
                            done("Error dose Not match requirement ");
                        }
                    }
                    done();
                } else {
                    done('Error');
                }
            }, function (err) {
                if (err === '') {
                    done('Server down.');
                } else {
                    done(err);
                }
            });
        });

         it('NS.Subject.fetch ::  Should fetch  subject with passing two parameters Id & Semester', function (done) {
            this.timeout(10000);
            // id, schoolId, deptId, semester, type
            var id="IH500";
            var semester= 2;
           
            NS.Subject.fetch(id,semester).then(function (res) {
                if (res instanceof Array) {
                    for(var i=0;i<res.length;i++)
                    {
                        if(res[i].id==="IH500" && res[i].semester===2)
                        {

                        }
                        else{
                            done("Error dose Not match requirement ");
                        }
                    }
                    done();
                } else {
                    done('Error');
                }
            }, function (err) {
                if (err === '') {
                    done('Server down.');
                } else {
                    done(err);
                }
            });
        });


         it('NS.Subject.fetch ::  Should fetch  subject with passing two parameters SchoolId & type', function (done) {
            this.timeout(10000);
            // id, schoolId, deptId, semester, type
            var schoolId= "sosc123";
            var type = "core";
            NS.Subject.fetch(schoolId,type).then(function (res) {
                if (res instanceof Array) {
                    for(var i=0;i<res.length;i++)
                    {
                        if(res[i].schoolId==="sosc123" && res[i].type==="core")
                        {

                        }
                        else{
                            done("Error dose Not match requirement ");
                        }
                    }
                    done();
                } else {
                    done('Error');
                }
            }, function (err) {
                if (err === '') {
                    done('Server down.');
                } else {
                    done(err);
                }
            });
        });

         it('NS.Subject.fetch ::  Should fetch  subject with passing two parameters SchoolId & Semester', function (done) {
            this.timeout(10000);
            var schoolId= "sosc123";
            var semester= 2;
            NS.Subject.fetch(schoolId,semester).then(function (res) {
                if (res instanceof Array) {
                    for(var i=0;i<res.length;i++)
                    {
                        if(res[i].schoolId==="sosc123" && res[i].semester===2)
                        {

                        }
                        else{
                            done("Error dose Not match requirement ");
                        }
                    }
                    done();
                } else {
                    done('Error');
                }
            }, function (err) {
                if (err === '') {
                    done('Server down.');
                } else {
                    done(err);
                }
            });
        });

         it('NS.Subject.fetch ::  Should fetch  subject with passing two parameters semester & type', function (done) {
            this.timeout(10000);
            // id, schoolId, deptId, semester, type
             var semester= 2;
             var type = "core";
            NS.Subject.fetch(semester ,type).then(function (res) {
                if (res instanceof Array) {
                    for(var i=0;i<res.length;i++)
                    {
                        if(res[i].semester===2 && res[i].type==="core")
                        {

                        }
                        else{
                            done("Error dose Not match requirement ");
                        }
                    }
                    done();
                } else {
                    done('Error');
                }
            }, function (err) {
                if (err === '') {
                    done('Server down.');
                } else {
                    done(err);
                }
            });
        });

          it('NS.Subject.fetch ::  Should fetch  subject with passing two parameters SchoolId & deptId', function (done) {
            this.timeout(10000);
            // id, schoolId, deptId, semester, type
            var schoolId= "sosc123";
            var deptId= "doh123";
            NS.Subject.fetch(schoolId,deptId).then(function (res) {
                if (res instanceof Array) {
                    for(var i=0;i<res.length;i++)
                    {
                        if(res[i].schoolId==="sosc123" && res[i].deptId==="doh123")
                        {

                        }
                        else{
                            done("Error dose Not match requirement ");
                        }
                    }
                    done();
                } else {
                    done('Error');
                }
            }, function (err) {
                if (err === '') {
                    done('Server down.');
                } else {
                    done(err);
                }
            });
        });

        //---------------------------------Create -------------------------//

        // it('NS.Subject.create :: Should Create new subject', function (done) {
        //     this.timeout(10000);
            
        //     var params = JSON.stringify({
        //         data: data
        //     })
        //     console.log('Fetching event ' + params);
        //     NS.Subject.create(params).then(function (res) {
        //         if (res instanceof Object) {
        //             done();
        //         } else {
        //             done('Error');
        //         }
        //     }, function (err) {
        //         if (err === '') {
        //             done('Server down.');
        //         } else {
        //             done(err);
        //         }
        //     });
        // });

// -----------------------* Get All Subjects from the database *---------------//


           it('NS.Subject.getAll ::  Should Get All Subjects from the database ', function (done) {
            this.timeout(10000);
           NS.Subject.getAll().then(function (res) {
                if (res instanceof Array && res.length>0 ) { // && res[0] instanceof NS.Subject
                    done();
                } else {
                    done('Error');
                }
            }, function (err) {
                if (err === '') {
                    done('Server down.');
                } else {
                    done(err);
                }
            });
        });

        //============================* fetchOpenCourses   *===============================//

        it('NS.Subject.fetchOpenCourses ::  Should Fetch the Open Courses for a specific Semester  ', function (done) {
            this.timeout(10000);
            var semester ={
                semester:2,
                isOpens:true
            }
           NS.Subject.fetchOpenCourses(semester).then(function (res) {
                if (res instanceof Array  ) { 
                    done();
                } else {
                    done('Error');
                }
            }, function (err) {
                if (err === '') {
                    done('Server down.');
                } else {
                    done(err);
                }
            });
        });

         //============================*   getFloatedCourses    *===============================//
        it('NS.Subject.getFloatedCourses ::  Should fetch all the subjects that have been floated for the particular course ', function (done) {
            this.timeout(20000);
             NS.Subject.getFloatedCourses().then(function (res) {
                //res.is of type NS.Event
                if (res instanceof Array && res.length > 0 ) {
                    NS._request().then(function (res2) {
                        if (res2.data instanceof Object) {
                            done(); 

                            } else {
                                    done('Expected Object, found ' + (typeof res2.data));
                        }
                    }, function (err2) {
                        done(err2);
                    });
                } else {
                    done('Not of type NS.Event');
                }
            }, function (err) {
                if (err === '') {
                    done('Server down.');
                } else {
                    done(err);
                }
            });
        });

         //============================* getSemesterData   *===============================//
        it('NS.Subject.getSemesterData ::  Should fetch the records of all the students enrolled for a certain course  ', function (done) {
            this.timeout(10000);
            //var key={id:"IH500"}
            var key="attendance";
            var courseId = "IH500";
           NS.Subject.getSemesterData(courseId,key).then(function (res) {
                // if (res instanceof Array   ) { //&& res.message==='Key is required.'
                //     done();
                // } else {
                //     done('Error');
                // }
                done();
            }, function (err) {
                if (err === '') {
                    done('Server down.');
                } else {
                    done(err);
                }
            });
        });
 
       //============================*   getFloatedCourses    *===============================//
        it('NS.Subject.getFloatedCourses ::  Should fetch all the subjects that have been floated for the particular course (time duration)', function (done) {
            this.timeout(20000);
             NS.Subject.getFloatedCourses().then(function (res) {
                //res.is of type NS.Event
                if (res instanceof Array && res.length > 0 ) {
                            done();                         
                       } else {
                       done('Not of type NS.Event');
                }
            }, function (err) {
                if (err === '') {
                    done('Server down.');
                } else {
                    done(err);
                }
            });
        });

         //============================* getSubjectCategories   *===============================//

        it('NS.Subject.getSubjectCategories ::  Should get the  different categories of the Subjects like Core or Elective  ', function (done) {
            this.timeout(10000);  
           NS.Subject.getSubjectCategories().then(function (res) {
                if (res instanceof Array && res.length>0 ) { 
                    done();
                } else {
                    done('Error');
                }
            }, function (err) {
                if (err === '') {
                    done('Server down.');
                } else {
                    done(err);
                }
            });
        });

         //============================*   floatOpenCourses    *===============================//
        it('NS.Subject.floatOpenCourses ::  Should used to float a subject', function (done) {
            this.timeout(20000);
            var subject={
                       
            }
            var offeredToArr=[];
             NS.Subject.floatOpenCourses().then(function (res) {
                //res.is of type NS.Event
                if (res instanceof Array && res.length > 0 ) {
                            done();                         
                       } else {
                       done('Not of type NS.Event');
                }
            }, function (err) {
                if (err === '') {
                    done('Server down.');
                } else {
                    done(err);
                }
            });
        });




});