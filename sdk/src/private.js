// Private Methods

NS._request = function (method, url, params, isFile) {
    var def = new NS.Promise();
    var xmlhttp = NS._loadXml();
    xmlhttp.open(method, url, true);
    if (!isFile) {
        xmlhttp.setRequestHeader('Content-Type', 'application/json');
    }

    //set session if user is logged In
    var token = localStorage.getItem('NSToken');
    if (token !== null) {
        xmlhttp.setRequestHeader('session', token);
    }

    if (NS.User.Current && NS.User.Current.document.currentRole) {
        var user = NS.User.Current;
        xmlhttp.setRequestHeader('avatar', user.document.currentRole.id);
    }

    if (params) {
        xmlhttp.send(params);
    } else {
        xmlhttp.send();
    }

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === xmlhttp.DONE) {
            if (xmlhttp.status === 200) {
                var session = xmlhttp.getResponseHeader('session');
                if (session) {
                    localStorage.setItem('NSToken', session);
                } else {
                    localStorage.removeItem('NSToken');
                }

                def.resolve(xmlhttp.responseText);
            } else {
                def.reject(xmlhttp.responseText);
            }
        }
    };
    return def;
};

//needs to be fixed

function _serialize(obj) {
    var nObj = obj.document;
    return nObj;
}


//type should be directly sent from the server

function _deserialize(obj, type) {
    var nObj;
    if (obj instanceof Array) {
        nObj = [];
        for (var i = 0; i < obj.length; i++) {
            nObj.push(_deserialize(obj[i], type));
        }
        return nObj;
    }
    if (type === 'user') {
        nObj = new NS.User();
        delete obj.type;
        nObj.document = obj;
    } else if (type === 'program') {
        nObj = new NS.Program(obj.id);
        nObj.document = obj;
    } else if (type === 'role') {
        nObj = new NS.Role(obj.id);
        nObj.document = obj;
    } else if (type === 'group') {
        nObj = new NS.Group(obj.id);
        nObj.document = obj;
    } else if (type === 'dissertation') {
        nObj = new NS.Dissertation(obj.id);
        nObj.document = obj;
    } else if (type === 'file') {
        nObj = new NS.File();
        nObj.document = obj;
    } else if (type === 'activity') {
        nObj = new NS.Activity(obj.id);
        nObj.document = obj;
    } else if (type === 'file') {
        nObj = new NS.File();
        nObj.document = obj;
    } else if (type === 'task') {
        nObj = new NS.Task(obj.id);
        nObj.document = obj;
        nObj.document.processType = _getApplicationType(obj.processDefinitionId);
    } else if (type === 'process') {
        nObj = new NS.Process(obj.id);
        nObj.document = obj;
        nObj.document.processType = _getApplicationType(obj.definitionId);
    } else if (type === 'employee') {
        nObj = new NS.Employee();
        nObj.document = obj;
    }
    else if (type === 'Asset') {
        nObj = new NS.Asset(obj);
    }
    else if (type === 'AssetFloor') {
        nObj = new NS.AssetFloor(obj);
    }
    else if (type === 'AssetRoom') {
        nObj = new NS.AssetRoom(obj);
    } else if (type === 'Allotment') {
        nObj = new NS.Allotment(obj);
    } else if (type === 'AssetAlloc') {
        nObj = new NS.AssetAlloc(obj);
    } else if (type === 'MessAccount') {
        nObj = new NS.MessAccount(obj);
    }

    return nObj;
}
function _serializeFile(type, obj) {
    return obj;
}

function _storeSession(user) {
    NS.User._setCurrentUser(user);
}

function _destroySession() {
    localStorage.removeItem('NSCurrentUser');
    localStorage.removeItem('NSToken');
    localStorage.clear();//clear everything from localStorage
    NS.User.Current = null;
}


function _setCurrentTask(task) {
    localStorage.setItem('currentTask', JSON.stringify(task.document));
}

function _getCurrentTask() {
    var task;
    if (localStorage.getItem('currentTask')) {
        task = new NS.Task();
        task.document = JSON.parse(localStorage.getItem('currentTask'));
    } else {
        task = null;
    }
    return task;
}

NS.currentTask = _getCurrentTask();


function _url() {
    var url = 'http://';
    if (window.location.hostname === 'egov.uohyd.ac.in' || window.location.hostname === '10.1.2.177') {
        url = url + window.location.host;
    } else {
        url = url + window.location.hostname;
        url = url + ':3200';
    }
    return url;
}

function _assetUrl() {
    var url = 'http://';
    url = url + window.location.hostname;
    url = url + ':8551';
    return url;
}

function _getApplicationType(definitionId) {
    var arr = null;
    if (definitionId) {
        arr = definitionId.split(':');
    } else {
        return '';
    }
    switch (arr[0]) {
        case 'semReg':
            return 'Semester Registration';
        case 'deRegistration':
            return 'De Registration';
        case 'noDues':
            return 'No Dues';
        case 'reRegistration':
            return 'Re Registration';
        case 'examReg':
            return 'Exam Registration';
        case 'phd_drc':
            return 'PhD DRC';
        case 'hostelChange':
            return 'Hostel Change';
    }
}
