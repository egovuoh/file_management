/**
 * @param  {String} id
 * @param  {Number} semester
 * @constructor
 *
 */
NS.Program = function (id, semester) {
    if (!id) {
        throw 'Program Id is Mandatory';
    }
    this.document = {};
    this.document.id = id;
    this.document.semester = semester;
    this._type = 'program';
};

/** @member {string} NS.Program.prototype#id */

Object.defineProperty(NS.Program.prototype, 'id', {
    get: function () {
        return this.document.id;
    },
    set: function (value) {
        this.document.id = value;
    }
});

/** @member {string} NS.Program.prototype#semester */

Object.defineProperty(NS.Program.prototype, 'semester', {
    get: function () {
        return this.document.semester;
    },
    set: function (value) {
        this.document.semester = value;
    }
});

/** @member {string} NS.Program.prototype#coreSub */

Object.defineProperty(NS.Program.prototype, 'coreSub', {
    get: function () {
        return this.document.coreSub;
    },
    set: function (value) {
        this.document.coreSub = value;
    }
});

/** @member {string} NS.Program.prototype#elective */

Object.defineProperty(NS.Program.prototype, 'elective', {
    get: function () {
        return this.document.elective;
    },
    set: function (value) {
        this.document.elective = value;
    }
});

/** @member {string} NS.Program.prototype#foundationSub */

Object.defineProperty(NS.Program.prototype, 'foundationSub', {
    get: function () {
        return this.document.foundationSub;
    },
    set: function (value) {
        this.document.foundationSub = value;
    }
});

/** @member {string} NS.Program.prototype#openElectiveSub */

Object.defineProperty(NS.Program.prototype, 'openElectiveSub', {
    get: function () {
        return this.document.openElectiveSub;
    },
    set: function (value) {
        this.document.openElectiveSub = value;
    }
});

/** @member {string} NS.Program.prototype#fees */

Object.defineProperty(NS.Program.prototype, 'fees', {
    get: function () {
        return this.document.fees;
    },
    set: function (value) {
        this.document.fees = value;
    }
});

/** @member {string} NS.Program.prototype#title */


Object.defineProperty(NS.Program.prototype, 'title', {
    get: function () {
        return this.document.progTitle;
    }
});

/** @member {string} NS.Program.prototype#courses */

Object.defineProperty(NS.Program.prototype, 'courses', {
    get: function () {
        return this.document.courses;
    }
});

/**
 * Fetch the courses for a Particular Program
 * @param  {Number} approvalLevel
 * @param  {Object} variables
 */
NS.Program.prototype.fetchCourses = function (approvalLevel, variables) {
    if (!this.document.id) {
        throw 'Program Id is required';
    }

    if (!this.document.semester) {
        throw 'Semester is required';
    }

    var def = new NS.Promise();
    var thisObj = this;
    var xmlhttp = NS._loadXml();
    var params = {
        programId: thisObj.document.id,
        semester: thisObj.document.semester,
        approvalLevel: approvalLevel
    };
    if (variables && variables.facultyId) {
        params.facultyId = variables.facultyId;
    }
    params = JSON.stringify(params);
    var url = NS.apiUrl + '/course/offered';
    NS._request('POST', url, params).then(function (response) {
        try {
            response = JSON.parse(response);
        } catch (e) {
            response = [];
        }
        thisObj.document.courses = response;
        def.resolve(thisObj);
    }, function (err) {
        def.reject(err);
    });
    return def;
};

/**
 * This fn only fetches those courses in which atleast student is registered.
 * Used in result generation triggering process.
 * Uses custom raw query, so please lookup the service before using.
 */

NS.Program.prototype.fetchActiveCourses = function () {

    if (!this.document.semester) {
        throw 'Semester is required';
    }
    var thisObj = this;
    var def = new NS.Promise();
    var xmlhttp = NS._loadXml();
    var params = {
        data: {
            programInstanceId: this.id,
            semester: this.semester,
        },
        key: 'ACTV_COURSES'
    };

    params = JSON.stringify(params);
    var url = NS.apiUrl + '/course/raw';
    NS._request('POST', url, params).then(function (response) {
        response = JSON.parse(response);
        thisObj.document.courses = response.data;
        def.resolve(thisObj);
    }, function (err) {
        try {
            err = JSON.parse(err);
        } catch (e) {
            err = err;
        }
        def.reject(err);
    });
    return def;
};


/* This funtions fetches the core subjects for a particular program and semester
 * Stores the response into the corresponding field.
 * @params : callback[optional]
 * @returns {*}
 */
NS.Program.prototype.fetchCoreCourses = function (callback) {

    if (!this.document.id) {
        throw 'Program Id is required';
    }

    if (!this.document.semester) {
        throw 'Semester is required';
    }

    var def;
    if (!callback) {
        def = new NS.Promise();
    }
    var thisObj = this;
    var xmlhttp = NS._loadXml();
    var params = JSON.stringify({
        programId: thisObj.document.id,
        semester: thisObj.document.semester,
        type: 'core',
        approvalLevel: 4
    });
    var url = NS.apiUrl + '/course/offered';
    NS._request('POST', url, params).then(function (response) {
        try {
            response = JSON.parse(response);
        } catch (e) {
            response = [];
        }
        thisObj.coreSub = response;
        if (callback) {
            callback.success(thisObj);
        } else {
            def.resolve(thisObj);
        }
    }, function (err) {
        if (callback) {
            callback.error(err);
        } else {
            def.reject(err);
        }
    });
    if (!callback) {
        return def;
    }
};

/* This funtions fetches the foundation subjects for a particular program and semester
 * Stores the response into the corresponding field.
 * @returns {*}
 */
NS.Program.prototype.fetchOpenCourses = function (type, student) {

    if (!this.document.id) {
        throw 'Program Id is required';
    }

    if (!this.document.semester) {
        throw 'Semester is required';
    }

    var def = new NS.Promise();
    var thisObj = this;
    var xmlhttp = NS._loadXml();
    var params = JSON.stringify({
        type: type,
        approvalLevel: 4,
        isOpen: true,
        eligibleForOpenCourse: {
            programId: student.programId,
            semester: student.semester,
            schoolId: student.schoolId
        }
    });
    var url = NS.apiUrl + '/course/offered';
    NS._request('POST', url, params).then(function (response) {
        try {
            response = JSON.parse(response);
        } catch (e) {
            response = [];
        }
        if (type === 'foundation') {
            thisObj.foundationSub = response;
        }else {
            thisObj.openElectiveSub = response;
        }
        
        def.resolve(thisObj);
    }, function (err) {
        def.reject(err);
    });
    return def;
};

/* This funtions fetches the elective subjects for a particular program and semester
 * Stores the response into the corresponding field.
 * @params : callback[optional]
 * @returns {*}
 */
NS.Program.prototype.fetchElective = function (callback) {

    var def;
    if (!callback) {
        def = new NS.Promise();
    }
    var thisObj = this;
    var xmlhttp = NS._loadXml();
    var params = JSON.stringify({
        programId: thisObj.document.id,
        semester: thisObj.document.semester,
        type: 'elective',
        approvalLevel: 4
    });
    var url = NS.apiUrl + '/course/offered';
    NS._request('POST', url, params).then(function (response) {
        try {
            response = JSON.parse(response);
        } catch (e) {
            response = [];
        }
        thisObj.elective = response;
        if (callback) {
            callback.success(thisObj);
        } else {
            def.resolve(thisObj);
        }
    }, function (err) {
        if (callback) {
            callback.error(err);
        } else {
            def.reject(err);
        }
    });
    if (!callback) {
        return def;
    }
};

/* This funtions fetches the fees for a particular program and semester
 * Stores the response into the corresponding field.
 * @params : callback[optional]
 * @returns {*}
 */
NS.Program.prototype.fetchFees = function (callback) {

    var def;
    if (!callback) {
        def = new NS.Promise();
    }
    var thisObj = this;
    var xmlhttp = NS._loadXml();
    var params = JSON.stringify({
        programId: thisObj.document.id
    });
    //var url = NS.apiUrl + '/academic/fees';
    var url = NS.apiUrl + '/finance/student/programfee';
    NS._request('POST', url, params).then(function (response) {
        response = JSON.parse(response);
        thisObj.fees = response;
        if (callback) {
            callback.success(thisObj);
        } else {
            def.resolve(thisObj);
        }
    }, function (err) {
        if (callback) {
            callback.error(err);
        } else {
            def.reject(err);
        }
    });
    if (!callback) {
        return def;
    }
};

/**
 * Offer Course to a Particular Program,takes the parameter from the program object
 */
NS.Program.prototype.offerCourse = function () {
    var def = new NS.Promise();
    var params = JSON.stringify({
        programId: this.id,
        subjects: this.subjects,
        semester: this.semester,
        deptId: NS.User.Current.currentRole.dept.id
    });
    var url = NS.apiUrl + '/course/offer';
    NS._request('POST', url, params).then(function (response) {
        def.resolve(response);
    }, function (err) {
        def.reject(err);
    });
    return def;
};

/**
 * Offer Course to a Particular Program,takes the parameter from the program object
 */
NS.Program.offerOpenCourse = function (courses, deptId, semester) {
    var def = new NS.Promise();
    var params = JSON.stringify({
        course: courses,
        semester: semester,
        deptId: deptId
    });
    var url = NS.apiUrl + '/course/offer/open';
    NS._request('POST', url, params).then(function (response) {
        def.resolve(response);
    }, function (err) {
        def.reject(err);
    });
    return def;
};

/**
 * Approve the Floated Course for a Program
 */

NS.Program.prototype.approveCourse = function () {
    var def = new NS.Promise();
    var params = JSON.stringify({
        subjects: this.subjects,
        role: {
            name: NS.User.Current.roleName,
            id: NS.User.Current.roleId,
            dept: NS.User.Current.currentRole.dept
        }
    });
    var url = NS.apiUrl + '/course/approve';
    NS._request('POST', url, params).then(function (response) {
        def.resolve(response);
    }, function (err) {
        def.reject(err);
    });
    return def;
};
/**
 * Update the Courses Whcih are already floated previously 
 * @param  {Array} courses
 * @param  {String} deptId
 */
NS.Program.prototype.updateFloatedCourses = function (courses, deptId) {
    var def = new NS.Promise();
    var params = JSON.stringify({
        programInstanceId: this.id,
        semester: this.semester,
        courses: courses,
        originDept: deptId
    });
    var url = NS.apiUrl + '/course/updateFloatedCourses';
    NS._request('POST', url, params).then(function (response) {
        def.resolve(response);
    }, function (err) {
        def.reject(err);
    });
    return def;
};

/**
 * Get the List of Programs which are there in the system 
 * Clarify whether this is for Program or Program Instance
 * @param  {} deptId
 */
NS.Program.getProgrammes = function (deptId) {
    var def = new NS.Promise();
    var params = JSON.stringify({
        deptId: deptId
    });
    var url = NS.apiUrl + '/program/get';
    NS._request('POST', url, params).then(function (response) {
        def.resolve(JSON.parse(response));
    }, function (err) {
        def.reject(err);
    });
    return def;
};

/**
 * Get the Master List of Programs
 * @param  {Object} filters
 */
NS.Program.getProgramsMasterData = function (filters) {
    var def = new NS.Promise();
    var params = JSON.stringify({
        filters: filters
    });
    var url = NS.apiUrl + '/program/get/masterdata';
    NS._request('POST', url, params).then(function (response) {
        def.resolve(JSON.parse(response));
    }, function (err) {
        def.reject(err);
    });
    return def;
};
/**
 * Update information about the Master Programs
 * @param  {Object} data
 */
NS.Program.updateProgramsMasterData = function (data) {
    var def = new NS.Promise();
    var params = JSON.stringify({
        data: data
    });
    var url = NS.apiUrl + '/program/update/masterdata';
    NS._request('PUT', url, params).then(function (response) {
        def.resolve(JSON.parse(response));
    }, function (err) {
        try {
            err = JSON.parse(err);
        } catch (e) {
            err = err;
        }
        def.reject(err);
    });
    return def;
};
/**
 * Create New Program Master
 * @param  {object} data
 */
NS.Program.insertProgramsMasterData = function (data) {
    var def = new NS.Promise();
    if (!data.createdBy) {
        data.createdBy = NS.User.Current.id;
    }
    var params = JSON.stringify({
        data: data
    });
    var url = NS.apiUrl + '/program/insert/masterdata';
    NS._request('POST', url, params).then(function (response) {
        def.resolve(JSON.parse(response));
    }, function (err) {
        try {
            err = JSON.parse(err);
        } catch (e) {
            err = err;
        }
        def.reject(err);
    });
    return def;
};

/**
 * Create Program Instance
 * @param  {Object} progData
 */

NS.Program.createInstance = function (progData) {
    var def = new NS.Promise();
    var params = JSON.stringify({
        progData: progData,
        user: {
            id: NS.User.Current.id
        }
    });
    var url = NS.apiUrl + '/program/create/instance';
    NS._request('POST', url, params).then(function (response) {
        def.resolve(JSON.parse(response));
    }, function (err) {
        def.reject(err);
    });
    return def;
};
/**
 * @description Get the results for a specific program at specified time in a semester
 * @param {string} key Available keys: 'result', 'resultWStudent', 'resultWProgram', 'resultWStudentWProgram'
 */
NS.Program.prototype.getResults = function (key) {
    var def = new NS.Promise();
    var params = JSON.stringify({
        filter: {
            programInstanceId: this.id,
            semester: this.semester
        },
        select: key,
        join: key
    });
    var url = NS.apiUrl + '/academic/result/get';
    NS._request('POST', url, params).then(function (response) {
        response = JSON.parse(response);
        def.resolve(response);
    }, function (err) {
        try {
            err = JSON.parse(err);
        } catch (e) {
            err = err;
        }
        def.reject(err);
    });
    return def;
};
/**
 * @description Generates the result for a given program(program instance)
 * @param {string} type Available types: 'regular', 'suppli', 'recourse', 'improvement'
 * NOTE: The program instance must have id and semester fields. Otherwise error will be thrown.
 */
NS.Program.prototype.generateResults = function (type) {
    var def = new NS.Promise();
    if (!(this.id && this.semester)) {
        def.reject({
            message: 'Program Id and semester are required'
        });
        return def;
    }
    if (!type) {
        def.reject({
            message: 'type is required'
        });
        return def;
    }
    var params = JSON.stringify({
        params: {
            programInstanceId: this.id,
            semester: this.semester,
            type: type
        },
    });
    var url = NS.apiUrl + '/academic/result/exam/generate';
    NS._request('POST', url, params).then(function (response) {
        response = JSON.parse(response);
        def.resolve(response);
    }, function (err) {
        try {
            err = JSON.parse(err);
        } catch (e) {
            err = err;
        }
        def.reject(err);
    });
    return def;
};
