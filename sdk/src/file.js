/**
 *
 */

NS.File = function () {
    this.document = {};
    this.document.type = 'file';
    this.document.id = null;
    this.document.name = null;
    this.document.version = null;
    this.document.tags = [];
    this.document.sha1 = null;
    this.document.description = null;
    this.document.size = 0;
    this.document.createdAt = null;
    this.document.updatedAt = null;
    this.document.trashedAt = null;
    this.document.purgedAt = null;
    this.document.createdBy = null;
    this.document.updatedBy = null;
    this.document.ownedBy = {};
    this.document.shareLink = null;
    /*{ "url": "https://www.box.com/s/rh935iit6ewrmw0unyul",
        "downloadUrl": "https://www.box.com/shared/static/rh935iit6ewrmw0unyul.jpeg",
        "vanityUrl": null,
        "isPasswordEnabled": false,
        "password":
        "unsharedAt": null,
        "downloadCount": 0,
        "previewCount": 0,
        "access": "open",
        "permissions": {
            "can_download": true,
            "can_preview": true
        }
      }*/

    this.document.status = null; //{active, trashed, deleted}
    this.document.oldVersions = [];
    this.document.extention = null;
    this.document.metadata = {};
    this.document.embedLink = {};
    this.document.access = {}; 
    this.document.isExternallyOwned = null;
    this.document.mimetype = null;
    this.document.originalname = null;
};

/* @name NS.File#id */
Object.defineProperty(NS.File.prototype, 'id', {
    get: function () {
        return this.document.id;
    },
    set: function (value) {
        this.document.id = value;
    }
});

/* @name NS.File#size */

Object.defineProperty(NS.File.prototype, 'size', {
    get: function () {
        return this.document.size;
    },
    set: function (value) {
        this.document.size = value;
    }
});

/* @name NS.File#type */

Object.defineProperty(NS.File.prototype, 'type', {
    get: function () {
        return this.document.type;
    },
    set: function (value) {
        this.document.type = value;
    }
});

/* @name NS.File#name */

Object.defineProperty(NS.File.prototype, 'name', {
    get: function () {
        return this.document.name;
    }, set: function (value) {
        this.document.name = value;
    }

});

/*  @name NS.File#mimetype */

Object.defineProperty(NS.File.prototype, 'mimetype', {
    get: function () {
        return this.document.mimetype;
    },
    set: function (value) {
        this.document.mimetype = value;
    }
});

/*  @name NS.File#originalname */

Object.defineProperty(NS.File.prototype, 'originalname', {
    get: function () {
        return this.document.originalname;
    }
});

/*  @name NS.File#description */

Object.defineProperty(NS.File.prototype, 'description', {
    get: function () {
        return this.document.description;
    },
    set: function (value) {
        this.document.description = value;
    }
});

/*  @name NS.File#size */

Object.defineProperty(NS.File.prototype, 'size', {
    get: function () {
        return this.document.size;
    }
});

/*  @name NS.File#createdAt */

Object.defineProperty(NS.File.prototype, 'createdAt', {
    get: function () {
        return this.document.createdAt;
    }
});

/*  @name NS.File#updatedAt */

Object.defineProperty(NS.File.prototype, 'updatedAt', {
    get: function () {
        return this.document.updatedAt;
    }
});

/*  @name NS.File#trashedAt */

Object.defineProperty(NS.File.prototype, 'trashedAt', {
    get: function () {
        return this.document.trashedAt;
    }
});

/*  @name NS.File#purgedAt */

Object.defineProperty(NS.File.prototype, 'purgedAt', {
    get: function () {
        return this.document.purgedAt;
    }
});

/*  @name NS.File#createdBy */

Object.defineProperty(NS.File.prototype, 'createdBy', {
    get: function () {
        return this.document.createdBy;
    }
});

/*  @name NS.File#updatedBy */

Object.defineProperty(NS.File.prototype, 'updatedBy', {
    get: function () {
        return this.document.updatedBy;
    }
});

/*  @name NS.File#ownedBy */

Object.defineProperty(NS.File.prototype, 'ownedBy', {
    get: function () {
        return this.document.ownedBy;
    },
    set: function (value) {
        this.document.ownedBy = value;
    }
});

/*  @name NS.File#sharedLink */

Object.defineProperty(NS.File.prototype, 'sharedLink', {
    get: function () {
        return this.document.sharedLink;
    },
    set: function (value) {
        this.document.sharedLink = value;
    }
});

/*  @name NS.File#status */

Object.defineProperty(NS.File.prototype, 'status', {
    get: function () {
        return this.document.status;
    },
    set: function (value) {
        var valid = ['active', 'trashed', 'purged'];
        if (!value) {
            this.document.status = [];
        } else if (value && valid.indexOf(value)) {
            this.document.status = value;
        }else {
            throw 'Invalid option for status property of file';
        }
    }
});

/*  @name NS.File#oldVersions */

Object.defineProperty(NS.File.prototype, 'oldVersions', {
    get: function () {
        return this.document.oldVersions;
    }
});

/*  @name NS.File#extention */

Object.defineProperty(NS.File.prototype, 'extention', {
    get: function () {
        return this.document.extention;
    },
    set: function (value) {
        this.document.extention = value;
    }
});

/*  @name NS.File#embededLink */

Object.defineProperty(NS.File.prototype, 'embededLink', {
    get: function () {
        return this.document.embededLink;
    },
    set: function (value) {
        this.document.embededLink = value;
    }
});

/*  @name NS.File#access */

Object.defineProperty(NS.File.prototype, 'access', {
    get: function () {
        return this.document.access;
    },
    set: function (value) {
        this.document.access = value;
    }
});

/*  @name NS.File#isExternallyOwned */

Object.defineProperty(NS.File.prototype, 'isExternallyOwned', {
    get: function () {
        return this.document.isExternallyOwned;
    },
    set: function (value) {
        this.document.isExternallyOwned = value;
    }
});


NS.File.prototype.get = function (fileId) {
    
};

NS.File.prototype.get = function () {

};

NS.File.prototype.upload = function () {

};

NS.File.prototype.download = function(){

};


NS.File.prototype.delete = function () {

    var def = new NS.Promise();
    var thisObj = this;

    if (!this.fileObj) {
        throw 'file object not found';
    }

    var xmlhttp = NS._loadXml();
    var params = JSON.stringify({
        fileObj: thisObj.document
    });

    var url = NS.apiUrl + '/file/delete';

    NS._request('POST', url, params).then(function (response) {
        response = JSON.parse(response);
        thisObj = null;
        def.resolve(response);
    }, function (err) {
        def.reject(err);
    });

    return def;
};

NS.File.delete = function (fileObj) {

    var def = new NS.Promise();

    var xmlhttp = NS._loadXml();
    var params = JSON.stringify({
        fileObj: fileObj.document
    });

    var url = NS.apiUrl + '/file/delete';

    NS._request('POST', url, params).then(function (response) {
        response = JSON.parse(response);
        def.resolve(response);
    }, function (err) {
        def.reject(err);
    });

    return def;

};

//----- File Access Functions ----
NS.File.prototype.embeddedLink = function() {

};

NS.File.prototype.shareLink = function() {

};

NS.File.prototype.createShareLink = function(password) {
    
};

NS.File.prototype.groupAccess = function(groupIds) {

};

NS.File.prototype.userAccess = function(userIds) {

};

//---- File Version Functions ----
NS.File.prototype.getVersion = function(versionNumber) {

};

NS.File.prototype.getAllVersions = function() {

};

NS.File.prototype.deleteVersion = function(versionNumber) {

};

NS.File.prototype.deleteOldVersions = function() {

};

//---- File Representation Functions ----
NS.File.prototype.thumbnail = function() {
    
};

NS.File.prototype.preview = function() {

};

NS.File.prototype.image = function() {

};

NS.File.prototype.original = function() {

};

//---- Trash Management ----
//1. Move to Trash
//2. Restore From Trash
//3. Permanent Delete From Trash


//---- MetaData Management ----
//1. Create Metadata for a file
//2. Update Metadata for a file
//3. Get Metadata for a file
//4. Delete Metadata for a file

//---- Search ----
//1. between dates
//2. filenames
//3. metadata

