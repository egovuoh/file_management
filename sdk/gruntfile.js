module.exports = function (grunt) {

    var pjson = grunt.file.readJSON('package.json');

    var config = {
        concat: {
            sdk: {
                src: [
                    'src/sdk.js',
                    'src/constants.js',
                    'src/file.js',
                    'src/private.js'
                ],
                dest: 'dist/1.0.0.js'
            }
        },
        uglify: {
            my_target: {
                files: {
                    'dist/1.0.0.min.js': ['dist/1.0.0.js']
                }
            }
        },
        jsdoc: {
            dist: {
                src: ['dist/1.0.0.js'],
                dest: 'doc'
            }
        }
    };
    grunt.initConfig(config);
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-jsdoc');
    grunt.registerTask('default', ['concat:sdk', 'uglify', 'jsdoc']);
};
