var http = require('http');
var express = require('express');
var app = express();
var bodyParser = require('body-parser');

app.use('/doc', express.static(__dirname + '/doc'));
app.use('/sdk', express.static(__dirname + '/dist'));
app.use(express.static(__dirname + '/tests'));

//Ending
app.set('port', process.env.PORT || 50000);
app.listen(app.get('port'), function (req, res) {
    console.log("Javascript SDK started on PORT:" + app.get('port'));
});
